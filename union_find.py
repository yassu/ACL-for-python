class UnionFind:
    n = 1
    parent_or_size = [-1 for i in range(n)]

    def __init__(self, N: int):
        self.n = N
        self.parent_or_size = [-1 for i in range(N)]

    def union(self, a: int, b: int) -> int:
        assert 0 <= a < self.n, "0<=a<n,a={0},n={1}".format(a, self.n)
        assert 0 <= b < self.n, "0<=b<n,b={0},n={1}".format(b, self.n)
        x = self.find_root(a)
        y = self.find_root(b)
        if x == y:
            return x
        if (-self.parent_or_size[x] < -self.parent_or_size[y]):
            x, y = y, x
        self.parent_or_size[x] += self.parent_or_size[y]
        self.parent_or_size[y] = x
        return x

    def same(self, a: int, b: int) -> bool:
        assert 0 <= a < self.n, "0<=a<n,a={0},n={1}".format(a, self.n)
        assert 0 <= b < self.n, "0<=b<n,b={0},n={1}".format(b, self.n)
        return self.find_root(a) == self.find_root(b)

    def find_root(self, a: int) -> int:
        assert 0 <= a < self.n, "0<=a<n,a={0},n={1}".format(a, self.n)
        if (self.parent_or_size[a] < 0):
            return a
        self.parent_or_size[a] = self.find_root(self.parent_or_size[a])
        return self.parent_or_size[a]

    def size(self, a: int) -> int:
        assert 0 <= a < self.n, "0<=a<n,a={0},n={1}".format(a, self.n)
        return -self.parent_or_size[self.find_root(a)]

    def groups(self) -> list:
        leader_buf = [0 for i in range(self.n)]
        group_size = [0 for i in range(self.n)]
        for i in range(self.n):
            leader_buf[i] = self.find_root(i)
            group_size[leader_buf[i]] += 1
        result = [[] for i in range(self.n)]
        for i in range(self.n):
            result[leader_buf[i]].append(i)
        result2 = []
        for i in range(self.n):
            if len(result[i]) > 0:
                result2.append(result[i])
        return result2

    def __len__(self):
        return self.n
